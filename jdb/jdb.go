package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/mattn/go-gtk/glib"
	"github.com/mattn/go-gtk/gtk"
)

var imgfile_logo string = filepath.Join("images", "go-gtk-logo.png")
var window *gtk.Window
var vbox *gtk.VBox
var menubar *gtk.MenuBar
var vpaned *gtk.VPaned
var frame1 *gtk.Frame
var framebox1 *gtk.VBox
var frame2 *gtk.Frame
var framebox2 *gtk.VBox
var lbl_hello *gtk.Label
var text_entry *gtk.Entry
var img_gtk_logo *gtk.Image
var scale *gtk.Scale
var buttonbox1 *gtk.HBox
var buttonbox2 *gtk.HBox
var buttonbox3 *gtk.HBox
var btn_generic *gtk.Button
var btn_font *gtk.FontButton
var btn_toggle *gtk.ToggleButton
var btn_check *gtk.CheckButton
var btn_radio_1 *gtk.RadioButton
var btn_radio_2 *gtk.RadioButton
var btn_radio_3 *gtk.RadioButton
var vsep *gtk.VSeparator
var radiobox *gtk.VBox
var cmb_animals *gtk.ComboBoxText
var cmb_fruit *gtk.ComboBoxText
var sw_hello *gtk.ScrolledWindow
var tw_hello *gtk.TextView
var statusbar *gtk.Statusbar

func add_frames_to_pane(v *gtk.VPaned, f1, f2 *gtk.Frame) { // {{{
	vpaned.Pack1(f1, false, false)
	vpaned.Pack2(f2, false, false)
} // }}}
func add_vbox_to_main_window(b *gtk.VBox, w *gtk.Window) { // {{{
	w.Add(b)
} // }}}
func create_main_window(title string) *gtk.Window { // {{{
	window := gtk.NewWindow(gtk.WINDOW_TOPLEVEL)
	window.SetPosition(gtk.WIN_POS_CENTER)
	window.SetTitle(title)
	window.SetIconName("gtk-dialog-info")
	return window
} // }}}
func create_vbox() *gtk.VBox { // {{{
	vbox := gtk.NewVBox(false, 1)
	return vbox
} // }}}
func create_main_vpane() *gtk.VPaned { // {{{
	vpaned := gtk.NewVPaned()
	return vpaned
} // }}}
func create_menubar() *gtk.MenuBar { // {{{
	menubar := gtk.NewMenuBar()
	//vbox.PackStart(menubar, false, false, 0)
	return menubar
} // }}}
func create_label(s string) *gtk.Label { // {{{
	l := gtk.NewLabel("Go Binding for GTK")
	l.ModifyFontEasy("DejaVu Serif 15")
	return l
} // }}}
func create_image(path string) *gtk.Image { // {{{
	i := gtk.NewImageFromFile(path)
	return i
} // }}}
func create_frame1() *gtk.Frame { // {{{
	frame1 := gtk.NewFrame("Frame 1")
	return frame1
} // }}}
func create_frame2() *gtk.Frame { // {{{
	frame2 := gtk.NewFrame("Frame 2")
	return frame2
} // }}}
func create_scale(max float64) *gtk.Scale { // {{{
	//scale := gtk.NewHScaleWithRange(0, float64(max), 1)
	scale := gtk.NewHScaleWithRange(0, max, 1)
	return scale
} // }}}
func create_hbox() *gtk.HBox { // {{{
	b := gtk.NewHBox(false, 1)
	return b
} // }}}
func create_generic_button(lbl string) *gtk.Button { // {{{
	b := gtk.NewButtonWithLabel(lbl)
	return b
} // }}}
func create_font_button() *gtk.FontButton { // {{{
	b := gtk.NewFontButton()
	return b
} // }}}
func create_toggle_button(lbl string) *gtk.ToggleButton { // {{{
	b := gtk.NewToggleButtonWithLabel(lbl)
	return b
} // }}}
func create_check_button(lbl string) *gtk.CheckButton { // {{{
	b := gtk.NewCheckButtonWithLabel(lbl)
	return b
} // }}}
func create_combo(entries ...string) *gtk.ComboBoxText {
	c := gtk.NewComboBoxText()
	for _, e := range entries {
		c.AppendText(e)
	}
	return c
}
func create_radio_button(grp *glib.SList, lbl string) *gtk.RadioButton { // {{{
	b := gtk.NewRadioButtonWithLabel(grp, lbl)
	return b
} // }}}
func create_textentry(s string) *gtk.Entry { // {{{
	t := gtk.NewEntry()
	t.SetText(s)
	return t
} // }}}
func create_textview(tstart, tend string) *gtk.TextView { // {{{
	t := gtk.NewTextView()
	var start, end gtk.TextIter
	buffer := t.GetBuffer()
	buffer.GetStartIter(&start)
	buffer.Insert(&start, tstart)
	buffer.GetEndIter(&end)
	buffer.Insert(&end, tend)
	tag := buffer.CreateTag("bold", map[string]string{
		"background": "#FF0000", "weight": "700"})
	buffer.GetStartIter(&start)
	buffer.GetEndIter(&end)
	buffer.ApplyTag(tag, &start, &end)
	return t
} // }}}
func create_scrolled_window() *gtk.ScrolledWindow { // {{{
	s := gtk.NewScrolledWindow(nil, nil)
	s.SetPolicy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
	s.SetShadowType(gtk.SHADOW_IN)
	return s
} // }}}
func create_statusbar(id, label string) *gtk.Statusbar { // {{{
	s := gtk.NewStatusbar()
	//context_id := s.GetContextId("go-gtk")
	//s.Push(context_id, "GTK binding for Go!")
	context_id := s.GetContextId(id)
	s.Push(context_id, label)
	return s
} // }}}
func fill_frame1(f *gtk.Frame, b *gtk.VBox) { // {{{
	b.Add(lbl_hello)
	b.Add(text_entry)
	b.Add(img_gtk_logo)
	f.Add(b)
} // }}}
func fill_frame2(f *gtk.Frame, b *gtk.VBox) { // {{{
	b.Add(scale)
	b.PackStart(buttonbox1, false, false, 0)
	b.PackStart(buttonbox2, false, false, 0)
	b.PackStart(buttonbox3, false, false, 0)
	b.PackStart(vsep, false, false, 0)
	b.Add(sw_hello)
	b.PackStart(statusbar, false, false, 0)
	f.Add(b)
} // }}}
func configure_window_destroy(w *gtk.Window) { // {{{
	// This fires when the user closes the window with the X button, but not
	// when MainQuit() gets called, as when they do File... Exit.
	w.Connect("destroy", func(ctx *glib.CallbackContext) {
		fmt.Println("got destroy!", ctx.Data().(string))
		gtk.MainQuit()
	}, "foo")
} // }}}
func configure_menubar(mb *gtk.MenuBar) { // {{{
	// File menu
	menu_file := gtk.NewMenuItemWithMnemonic("_File")
	mb.Append(menu_file)

	// File... Exit
	file_submenu := gtk.NewMenu()
	menu_file.SetSubmenu(file_submenu)
	var file_exit *gtk.MenuItem
	file_exit = gtk.NewMenuItemWithMnemonic("E_xit")
	file_exit.Connect("activate", func() {
		gtk.MainQuit()
	})
	file_submenu.Append(file_exit)

	// View menu
	menu_view := gtk.NewMenuItemWithMnemonic("_View")
	mb.Append(menu_view)
	view_submenu := gtk.NewMenu()
	menu_view.SetSubmenu(view_submenu)

	// View... Disable
	view_disable := gtk.NewCheckMenuItemWithMnemonic("_Disable")
	view_disable.Connect("activate", func() {
		vpaned.SetSensitive(!view_disable.GetActive())
	})
	view_submenu.Append(view_disable)

	/*

		In the demo, this is changing the font, and putting the new font name on a
		button label.  I haven't got that button placed yet, so this isn't going
		to work.

		I can
			- Put this configure_menubar() at the bottom of all the rest of the
			  code and pass it every button and frame etc that it needs to modify
			- Break configure_menubar() up into pieces (configure_menubar_file(),
			  configure_menubar_view(), etc) and just pass in the pieces that each
			  function needs
			- Make all of the individual buttons and bits of GUI into globals and
			  not pass them in at all


		// View... Font
		view_font = gtk.NewMenuItemWithMnemonic("_Font")
		view_font.Connect("activate", func() {
			fsd := gtk.NewFontSelectionDialog("Font")
			fsd.SetFontName(fontbutton.GetFontName())
			fsd.Response(func() {
				fmt.Println(fsd.GetFontName())
				fontbutton.SetFontName(fsd.GetFontName())
				fsd.Destroy()
			})
			fsd.SetTransientFor(window)
			fsd.Run()
		})
		view_submenu.Append(view_font)

	*/
} // }}}
func configure_scale(s *gtk.Scale) { // {{{
	s.Connect("value-changed", func() {
		fmt.Println("scale:", int(scale.GetValue()))
	})
} // }}}
func main() {
	gtk.Init(&os.Args)

	// Create various bits of GUI
	window = create_main_window("JDB Test")
	menubar = create_menubar()
	lbl_hello = create_label("Go Binding for GTK")
	text_entry = create_textentry("This is a text entry.")
	img_gtk_logo = create_image(imgfile_logo)
	scale = create_scale(100)
	btn_generic = create_generic_button("Button with label")
	btn_toggle = create_toggle_button("ToggleButton with label")
	btn_font = create_font_button()
	btn_check = create_check_button("CheckButton with label")
	btn_radio_1 = create_radio_button(nil, "Radio1")
	btn_radio_2 = create_radio_button(btn_radio_1.GetGroup(), "Radio2")
	btn_radio_3 = create_radio_button(btn_radio_1.GetGroup(), "Radio3")
	vsep = gtk.NewVSeparator()
	cmb_animals = create_combo("Monkey", "Tiger", "Elephant")
	cmb_fruit = create_combo("Peach", "Banana", "Apple")
	tw_hello = create_textview("This is a ", "TextView")
	statusbar = create_statusbar("go-gtk", "GTK binding for Go!")

	// Horiz box with two buttons (incl font button)
	buttonbox1 = create_hbox()
	buttonbox1.Add(btn_generic)
	buttonbox1.Add(btn_font)

	// Vertical radio button box
	// We'll put this vertical box in the horiz box below
	radiobox = create_vbox()
	radiobox.Add(btn_radio_1)
	radiobox.Add(btn_radio_2)
	radiobox.Add(btn_radio_3)
	btn_radio_1.SetActive(true)

	// Box with toggle, check, and radio group
	buttonbox2 = create_hbox()
	buttonbox2.Add(btn_toggle)
	buttonbox2.Add(btn_check)
	buttonbox2.Add(radiobox)

	// Animals combo has no default (so don't touch it), but fruits combo
	// should default to Banana
	cmb_fruit.SetActive(1)

	// HBox with animals selectbox and fruit combobox
	buttonbox3 = create_hbox()
	buttonbox3.Add(cmb_animals)
	buttonbox3.Add(cmb_fruit)

	// Scrolled window containing HW! text entry
	sw_hello = create_scrolled_window()
	sw_hello.Add(tw_hello)

	// --------------------------------------------------------------------------------------------
	// GUI pieces are built.  Put them together.
	// --------------------------------------------------------------------------------------------
	// Frames containing the GUI pieces
	frame1 = create_frame1()
	framebox1 = create_vbox()
	fill_frame1(frame1, framebox1)
	frame2 = create_frame2()
	framebox2 = create_vbox()
	fill_frame2(frame2, framebox2)

	// Create main pane, add frames to it
	vpaned = create_main_vpane()
	add_frames_to_pane(vpaned, frame1, frame2)

	// Create main vertical box, add menubar and main pane to it
	vbox = create_vbox()
	vbox.PackStart(menubar, false, false, 0)
	vbox.Add(vpaned)

	// Add main vertical box to our window
	window.Add(vbox)

	// ----------------------------------------------------------
	// The entire GUI has been created and laid out.  NOW hook up
	// functionality.
	// ----------------------------------------------------------
	configure_window_destroy(window)
	configure_menubar(menubar)
	configure_scale(scale)

	//--------------------------------------------------------
	// Everything is built and configured, so show it.
	//--------------------------------------------------------
	window.SetSizeRequest(600, 600)
	window.ShowAll()
	gtk.Main()
}
